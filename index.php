<?php require "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP OOP | Activity</title>
</head>
<body>

<ul>
    <li>Print Product here</li>
    <?php 
        echo $newProduct->printDetails();
    ?>
    <li>Print a subproduct here</li>
    <?php 
        echo $newMobile->printDetails();
    ?>
    <li>Print another subproduct here</li>
    <?php 
        echo $newComputer->printDetails();
    ?>
</ul>
    
</body>
</html>