<?php 

    class Product {
        public $name;
        public $price;
        public $description;
        public $category;
        public $stockNo;

        public function __construct($nameV, $priceV, $descriptionV, $categoryV, $stockNoV){
            $this->name = $nameV;
            $this->price = $priceV;
            $this->description = $descriptionV;
            $this->category = $categoryV;
            $this->stockNo = $stockNoV;
        }

        function printDetails(){
            return "The product has a name of $this->name and its price is $this->price, and the stock no is $this->stockNo";
        }

        public function getPrice(){
            return $this->price;
        }

        public function setPrice($priceV) {
            $this->price = $priceV;
        }

        public function getStockNo(){
            return $this->stockNo;
        }

        public function setStockNo($stockNoV) {
            $this->stockNo = $stockNoV;
        }

        public function getCategory(){
            return $this->category;
        }

        public function setCategory($categoryV) {
            $this->category = $categoryV;
        }

    }

    $newProduct = new Product('Xioami Mi Monitor', '22,000.00', 'Good for gaming and for coding', 'computer-peripherals', '5');


    class Mobile extends Product {
        function printDetails(){
            return "Our latest mobile is $this->name with a cheapest price of $this->price";
        }
    }

    $newMobile = new Mobile('Xioami Redmi Note 10 pro', '13590.00', 'Latest Xioami phone ever made', 'mobiles and electronics', '10');

    

    class Computer extends Product {
        function printDetails(){
            return "Our newest computer is $this->name and its base price is $this->price";
        }
    }

    $newComputer = new Computer('HP Business Laptop', '29000.00', 'HP Laptop only made for business', 'laptops and computer', '10');
	
    $newProduct->setStockNo(3);
    $newMobile->setStockNo(5);
    $newComputer->setCategory('laptops, computers and electronics');

 ?>